# Cluster Autoscaler

Cluster Autoscaler will respect the minimum and maximum values of each Auto Scaling Group. It will only adjust the desired value.

Each Auto Scaling Group should be composed of instance types that provide approximately equal capacity. For example, ASG "xlarge" could be composed of m5a.xlarge, m4.xlarge, m5.xlarge, and m5d.xlarge instance types, because each of those provide 4 vCPUs and 16GiB RAM. Separately, ASG "2xlarge" could be composed of m5a.2xlarge, m4.2xlarge, m5.2xlarge, and m5d.2xlarge instance types, because each of those provide 8 vCPUs and 32GiB RAM.

Cluster Autoscaler will attempt to determine the CPU, memory, and GPU resources provided by an Auto Scaling Group based on the instance type specified in its Launch Configuration or Launch Template. It will also examine any overrides provided in an ASG's Mixed Instances Policy. If any such overrides are found, only the first instance type found will be used.


### Usage:
#### To deploy use Makefile: 

```
make deploy cluster=REPLACE_WITH_CLUSTER_NAME account=REPLACE_WITH_AWS_ACCOUNT_ID
```
or step-by-step as explained below

### Cluster Autoscaler with Auto-Discovery

Auto-Discovery is the preferred method to configure Cluster Autoscaler.
Cluster Autoscaler will attempt to determine the CPU, memory, and GPU resources provided by an Auto Scaling Group based on the instance type specified in its Launch Configuration or Launch Template.

### Configure the ASG

You configure the size of your Auto Scaling group by setting the minimum, maximum, and desired capacity. 
Make sure you desired capacity is NOT equal to maximum, itherwise autoscaler will not be triggered.

### IAM roles for service accounts

With IAM roles for service accounts on Amazon EKS clusters, you can associate an IAM role with a Kubernetes service account. This service account can then provide AWS permissions to the containers in any pod that uses that service account. With this feature, you no longer need to provide extended permissions to the node IAM role so that pods on that node can call AWS APIs.

Enabling IAM roles for service accounts on your cluster:

To deploy:

```
make oidc cluster=REPLACE_WITH_CLUSTER_NAME
```
### IAM policy

Creating an IAM policy for your service account that will allow your CA pod to interact with the autoscaling groups.

```
make iam_policy
```
### IAM role and Cluster Service Account
Create an IAM role for the cluster-autoscaler Service Account in the kube-system namespace.

```
make iam_service_account cluster=REPLACE_WITH_CLUSTER_NAME account=REPLACE_WITH_AWS_ACCOUNT_ID
```

#### *Optional step
Make sure your service account with the ARN of the IAM role is annotated

```
kubectl -n kube-system describe sa cluster-autoscaler

Name:                cluster-autoscaler
Namespace:           kube-system
Labels:              {some_string}
Annotations:         eks.amazonaws.com/role-arn: arn:aws:iam::{some_account}:role/{some_role}
Image pull secrets:  <none>
Mountable secrets:   {some_string}
Tokens:              {some_string}
Events:              <none>
```

### Deploy the Cluster Autoscaler (CA)

```
make deploy_autoscaler cluster=REPLACE_WITH_CLUSTER_NAME
```

To prevent CA from removing nodes where its own pod is running, we will add the "cluster-autoscaler.kubernetes.io/safe-to-evict" annotation to its deployment with the following command:

```
make patch_autoscaler
```

### To check the logs:

```
kubectl -n kube-system logs -f deployment/cluster-autoscaler
```

